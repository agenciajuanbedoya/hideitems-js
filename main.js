var date = new Date();          // Extraer datos de fecha
var hh = date.getHours();       // Asignar hora del día
var mm = date.getMonth() + 1;   // Asignar mes del año
var dm = date.getDate();        // Asignar dia del mes
var dw = date.getDay();         // Asignar dia de la semana
var yyyy = date.getFullYear(); // Asignar año

/*
// Formatear día del mes y mes del año
if (dm < 10) { dm = '0' + dm; }
if (mm < 10) { mm = '0' + mm; } */

switch (mm) {
    case 1:
        var month = "enero";
        break;
    case 2:
        var month = "febrero";
        break;
    case 3:
        var month = "marzo";
        break;
    case 4:
        var month = "abril";
        break;
    case 5:
        var month = "mayo";
        break;
    case 6:
        var month = "junio";
        break;
    case 7:
        var month = "julio";
        break;
    case 8:
        var month = "agosto";
        break;
    case 9:
        var month = "septiembre";
        break;
    case 10:
        var month = "octubre";
        break;
    case 11:
        var month = "noviembre";
        break;
    case 12:
        var month = "diciembre";
} 

// Formato de fecha
var dcToday = dm + " " + month + ", " + yyyy;

// URL de visita
var urlNow = window.location.href;

// Rastreo de datepicker
var dInyection
if (urlNow == "https://www.hachikopets.com/pagar/") {
    setInterval('var dateSelect = document.getElementById("delivery_date").value;', 100);
}

// Franjas horias
function checkStripes() {
    // Franja 1: no opera de 8am hasta las 12am del mismo dia (LUN - SAB)
    if ((hh >= 8 && hh <= 23) && (dw != 0)) {
        document.getElementsByTagName("label")[17].style.pointerEvents = "none";
        document.getElementsByTagName("label")[17].style.background = "#999";
        document.getElementsByTagName("label")[17].style.textDecoration = "line-through";
    } else { // Inabilitado
        document.getElementsByTagName("label")[17].style.pointerEvents = "auto";
        document.getElementsByTagName("label")[17].disabled = false;
    }
    // Franja 2: no opera de 12pm a 12am (LUN - SAB)
    if ((hh >= 12 && hh <= 23) && (dw != 0)) {
        document.getElementsByTagName("label")[18].style.pointerEvents = "none";
        document.getElementsByTagName("label")[18].style.background = "#999";
        document.getElementsByTagName("label")[18].style.textDecoration = "line-through";
    } else { // Inabilitado
        document.getElementsByTagName("label")[18].style.pointerEvents = "auto";
        document.getElementsByTagName("label")[18].disabled = false;
    }
    // Franja 3: no opera de 4pm a 12am (LUN - SAB)
    if ((hh >= 16 && hh <= 23) && (dw != 0)) {
        document.getElementsByTagName("label")[19].style.pointerEvents = "none";
        document.getElementsByTagName("label")[19].style.background = "#999";
        document.getElementsByTagName("label")[19].style.textDecoration = "line-through";
    } else { // Inabilitado
        document.getElementsByTagName("label")[19].style.pointerEvents = "auto";
        document.getElementsByTagName("label")[19].disabled = false;
    }
    // Franja 4 (Express): opera de 9am a 4pm (LUN - SAB)
    if ((hh >= 8 && hh <= 15) && (dw != 0)) {
        document.getElementsByTagName("label")[20].style.pointerEvents = "auto";
        document.getElementsByTagName("label")[20].disabled = false;
    } else { // Inabilitado
        document.getElementsByTagName("label")[20].style.pointerEvents = "none";
        document.getElementsByTagName("label")[20].style.background = "#999";
        document.getElementsByTagName("label")[20].style.textDecoration = "line-through";
    }
    // Desactivar boton de pago: opera desde las 4pm hasta las 12am (LUN-SAB)
    if ((hh >= 16 && hh <= 23) && (dw != 0)) {
        document.getElementById("place_order").style.display = "none";
    } else {
        document.getElementById("place_order").style.display = "block";
    }
}

function deactiveAll() {
    // Franja 1
    document.getElementsByTagName("label")[17].style.pointerEvents = "none";
    document.getElementsByTagName("label")[17].style.background = "#999";
    document.getElementsByTagName("label")[17].style.textDecoration = "line-through";
    // Franja 2
    document.getElementsByTagName("label")[18].style.pointerEvents = "none";
    document.getElementsByTagName("label")[18].style.background = "#999";
    document.getElementsByTagName("label")[18].style.textDecoration = "line-through";
    // Franja 3
    document.getElementsByTagName("label")[19].style.pointerEvents = "none";
    document.getElementsByTagName("label")[19].style.background = "#999";
    document.getElementsByTagName("label")[19].style.textDecoration = "line-through";
    // Franja Express
    document.getElementsByTagName("label")[20].style.pointerEvents = "none";
    document.getElementsByTagName("label")[20].style.background = "#999";
    document.getElementsByTagName("label")[20].style.textDecoration = "line-through";
    // Boton de pago
    document.getElementById("place_order").style.display = "none";
}

function deactiveExpressDelivery() {
    document.getElementsByTagName("label")[20].style.pointerEvents = "none";
    document.getElementsByTagName("label")[20].style.background = "#999";
    document.getElementsByTagName("label")[20].style.textDecoration = "line-through";
}

/* Ejecucion de ordenes */
function activeCode() {
    if (dateSelect == dcToday) {
        checkStripes();
    } else if ((dateSelect == "4 noviembre, 2019") || (dateSelect == "11 noviembre, 2019")) {
        deactiveAll()
    } else if (dateSelect == "") {
        deactiveAll()
    } else if (dateSelect != dcToday) {
        deactiveExpressDelivery()
    }
}

if (urlNow == "https://www.hachikopets.com/pagar/") {
    setInterval('activeCode()', 100);
}

console.log("Sitio web desarrollado por: Juan Bedoya @ 2019");
